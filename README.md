### What is this repository for? ###

A simple app to get a quick introduction to Android. The most basic description would be an Android version of the paper menu of the Lucabaps chip shop in Portadown.
The menu is accessed from a static webpage that stores JSON data.

### Scope for improvement ###

1) Adding images to each product
2) Changing static JSON webpage to either a webservice or a database.
3) Adding a online ordering system

### Play store location ###

https://play.google.com/store/apps/details?id=com.view.web.lucabapsapp&hl=en