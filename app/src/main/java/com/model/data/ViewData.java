package com.model.data;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Rory on 24/05/2015.
 */
public class ViewData {
    private List<String> listDataHeader;
    private HashMap<String, List<String>> listDataChild, listDataChildSub;

    public ViewData(List<String> listDataHeader, HashMap<String, List<String>> listDataChild, HashMap<String, List<String>> listDataChildSub) {
        this.listDataHeader = listDataHeader;
        this.listDataChild = listDataChild;
        this.listDataChildSub = listDataChildSub;
    }

    public List<String> getListDataHeader() {
        return listDataHeader;
    }

    public HashMap<String, List<String>> getListDataChild() {
        return listDataChild;
    }

    public HashMap<String, List<String>> getListDataChildSub() {
        return listDataChildSub;
    }
}
