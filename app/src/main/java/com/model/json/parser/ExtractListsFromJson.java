package com.model.json.parser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Rory on 08/05/2015.
 */
public class ExtractListsFromJson {
    private String menuName;
    private List<String> headers;
    private Map<String, List<String>> children;
    private Map<String, List<String>> childrenSub;
    private List<String> descriptionList;
    private List<String> childrenList;
    private List<String> childrenSubList;
    private static JSONObject json;

    protected ExtractListsFromJson(String menuName) {
        this.menuName = menuName;
        headers = new ArrayList<>();
        children = new HashMap<>();
        childrenSub = new HashMap<>();
        descriptionList = new ArrayList<>();
        childrenList = new ArrayList<>();
        childrenSubList = new ArrayList<>();
    }

    public void prepareData() {
        try {
        	JSONArray jsonArray = json.getJSONArray("Menus");
        	JSONArray menu = new JSONArray();
            switch (menuName) {
                case "Main Menu":
                    menu = jsonArray.getJSONObject(0).getJSONArray("Main");
                    break;
                case "Chinese Menu":
                	menu = jsonArray.getJSONObject(1).getJSONArray("Chinese");
                    break;
                case "Pizza Menu":
                	menu = jsonArray.getJSONObject(2).getJSONArray("Pizza");
                    break;
                default:
                    break;
            }
            extractHeaders(menu);
            extractChildren(menu);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void extractHeaders(JSONArray menusJson) throws JSONException {
        JSONObject headersObj = menusJson.getJSONObject(0);
        JSONArray headersArray = headersObj.getJSONArray("Headers");
        JSONObject element;
        for (int x = 0; x < headersArray.length(); x++) {
        	element = (JSONObject) headersArray.get(x);
            headers.add((String) element.getString("header"));
        }
    }

    private void extractChildren(JSONArray menusJson) throws JSONException {
        JSONObject childrenObj = menusJson.getJSONObject(1);
        JSONArray childenArray = childrenObj.getJSONArray("Descriptions");
        JSONObject elementsTogether;
        JSONArray elementArray;
        JSONObject element;
        elementsTogether = (JSONObject) childenArray.get(0);

        for (int x = 0; x < headers.size(); x++) {
        	String headerName = headers.get(x);
        	elementArray = (JSONArray) elementsTogether.getJSONArray(headerName);
            for (int m = 0; m < elementArray.length(); m++) {
            	element = (JSONObject) elementArray.getJSONObject(m);
            	descriptionList.add((String) element.getString("description"));
                descriptionList.add((String) element.getString("price"));
            }
            for (int t = 0; t < descriptionList.size(); t=t+2) {
                childrenList.add(descriptionList.get(t));
                childrenSubList.add(descriptionList.get(t+1));
            }
            children.put(headerName, new ArrayList<String>(new ArrayList<String>(childrenList)));
            childrenSub.put(headerName, new ArrayList<String>(new ArrayList<String>(childrenSubList)));
            clearChildrenLists();
        }
    }
    
    private void clearChildrenLists() {
    	descriptionList.clear();
    	childrenList.clear();
    	childrenSubList.clear();
    }

    protected static void setJson(JSONObject jsonFromWeb) {
        ExtractListsFromJson.json = jsonFromWeb;
    }

    public List<String> getHeaders() {
        return headers;
    }

    public Map<String, List<String>> getChildren() {
        return children;
    }

    public Map<String, List<String>> getChildrenSub() {
        return childrenSub;
    }
}
