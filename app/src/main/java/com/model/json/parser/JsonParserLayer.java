package com.model.json.parser;

import android.content.Context;

import com.file.i.o.FileLayer;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by Rory on 08/05/2015.
 */
public class JsonParserLayer {
    private ExtractListsFromJson jsonData;

    public JsonParserLayer(String url) throws JSONException, IOException {
        ReadJsonFromWebPage jReader = new ReadJsonFromWebPage(url);
        jReader.startProcess();
    }

    public JsonParserLayer(String filename, Context ctx) throws JSONException, IOException {
        JSONObject json = FileLayer.ReadFile(filename, ctx);
        ExtractListsFromJson.setJson(json);
    }

    public ExtractListsFromJson getJsonData(String menuName) {
        jsonData = new ExtractListsFromJson(menuName);
        return jsonData;
    }
}
