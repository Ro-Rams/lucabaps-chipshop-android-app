package com.listeners;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.view.web.lucabapsapp.R;

import java.util.HashMap;
import java.util.List;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Fragment _context;
    private List<String> _listDataHeader;
    private HashMap<String, List<String>> _listDataChild, _listDataChildSub;
    private ExpandableListView _expListView;
    private int[] groupStatus;
    private int lastExpandedGroupPosition;

    public ExpandableListAdapter(Fragment context, ExpandableListView expListView, List<String> listDataHeader,
                                 HashMap<String, List<String>> listChildData, HashMap<String, List<String>> listDataChildSub) {
        this._expListView = expListView;
        this._listDataChildSub = listDataChildSub;
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        groupStatus = new int[listDataHeader.size()];
        setListEvent();
    }

    private void setListEvent() {

        _expListView
                .setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

                    public void onGroupExpand(int arg0) {
                        // TODO Auto-generated method stub
                        groupStatus[arg0] = 1;
                        lastExpandedGroupPosition = arg0;
                    }
                });

        _expListView
                .setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

                    public void onGroupCollapse(int arg0) {
                        // TODO Auto-generated method stub
                        groupStatus[arg0] = 0;
                    }
                });
    }

    @Override
    public void onGroupExpanded(int groupPosition){

        if(groupPosition != lastExpandedGroupPosition){
            _expListView.collapseGroup(lastExpandedGroupPosition);
        }

        super.onGroupExpanded(groupPosition);
        if (groupPosition > lastExpandedGroupPosition) {
            moveToSelection(groupPosition);
        }
        lastExpandedGroupPosition = groupPosition;
        _expListView.smoothScrollToPosition(groupPosition);
        _expListView.setSelection(groupPosition);
    }

    private void moveToSelection(int groupPosition) {
        int position = getPosition(groupPosition);
        _expListView.smoothScrollToPosition(0 - position);
        _expListView.setSelection(0 - position);
    }

    private int getPosition(int groupPosition) {
        String key = _listDataHeader.get(groupPosition);
        int position = _listDataChild.get(key).size();
        return position;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        String line = this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosition);
        if (line == null) {
            return "";
        } else {
            return line;
        }
    }

    public Object getChildSubtitle(int groupPosition, int childPosition) {
        String line = this._listDataChildSub.get(this._listDataHeader.get(groupPosition)).get(childPosition);
        if (line == null) {
            return "";
        } else {
            return line;
        }
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        final String childText = (String) getChild(groupPosition, childPosition);
        final String childTextSub = (String) getChildSubtitle(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getActivity()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_item, null);
        }

        TextView txtListChild = (TextView) convertView
                .findViewById(R.id.lblListItem);
        txtListChild.setText(childText);

        TextView txtListChildSub = (TextView) convertView.findViewById(R.id.lblListItemSub);
        txtListChildSub.setText(childTextSub);
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        String header = this._listDataHeader.get(groupPosition);
        return header;
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getActivity()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group, null);
        }

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.lblListHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    public boolean isChildSelectable(int arg0, int arg1) {
        // TODO Auto-generated method stub
        return true;
    }

}
