package com.file.i.o;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by Rory on 25/05/2015.
 */
public class FileLayer {

    private static JSONObject json;

    public static void setJsonObject(JSONObject json) {
        FileLayer.json = json;
    }

    public static void CreateFile(String filename, Context ctx) {
        WriteToFile fileWriter = new WriteToFile(json, ctx, filename);
        fileWriter.writeJsonToFile();
    }

    public static JSONObject ReadFile(String filename, Context ctx) throws IOException, JSONException {
        ReadFromFile fileReader = new ReadFromFile(filename, ctx);
        return fileReader.readJsonFromFile();
    }
}
