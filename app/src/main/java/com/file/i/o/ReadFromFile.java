package com.file.i.o;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Rory on 25/05/2015.
 */
class ReadFromFile {
    private static String filename;
    private Context ctx;

    protected ReadFromFile(String filename, Context ctx) {
        ReadFromFile.filename = filename;
        this.ctx = ctx;
    }

    public JSONObject readJsonFromFile() throws IOException, JSONException {
        String text = "";
        InputStream is = ctx.openFileInput(filename);
        int size = is.available();
        byte[] buffer = new byte[size];
        is.read(buffer);
        is.close();
        text = new String(buffer, "UTF-8");
        JSONObject json = new JSONObject(text);
        return json;
    }
}
