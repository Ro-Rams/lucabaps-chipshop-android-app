package com.file.i.o;

import android.content.Context;
import android.util.Log;

import org.json.JSONObject;

import java.io.FileOutputStream;
import java.io.IOException;

class WriteToFile {

    private static String filename;
    FileOutputStream outputStream;
    JSONObject json;
    Context ctx;

    WriteToFile(JSONObject json, Context ctx, String filename) {
        this.json = json;
        this.ctx = ctx;
        WriteToFile.filename = filename;
    }

    protected void writeJsonToFile() {
        try {
            String fileContents = json.toString();
            outputStream = ctx.openFileOutput(filename, Context.MODE_PRIVATE);
            outputStream.write(fileContents.getBytes());
            outputStream.close();
        } catch (IOException e) {
            Log.d("WriteToFile: ", e.getMessage());
        }
    }
}
