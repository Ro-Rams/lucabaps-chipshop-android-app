package com.view.web.lucabapsapp;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.listeners.ExpandableListAdapter;
import com.model.data.ViewData;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Rory on 04/05/2015.
 */
public class PizzaMenuFragmentTab extends Fragment {

    private ExpandableListAdapter listAdapter;
    private ExpandableListView expListView;
    private List<String> listDataHeader;
    private HashMap<String, List<String>> listDataChild, listDataChildSub;
    protected static boolean success;
    private static ViewData thisViewData;

    static {
        success = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.main_menu_fragment, container, false);
        expListView = (ExpandableListView) rootView.findViewById(R.id.lvExp);
        while (!success) {
        }
        listAdapter = new ExpandableListAdapter(this, expListView, PizzaMenuFragmentTab.thisViewData.getListDataHeader(), PizzaMenuFragmentTab.thisViewData.getListDataChild(), PizzaMenuFragmentTab.thisViewData.getListDataChildSub());
        expListView.setAdapter(listAdapter);
        return rootView;
    }

    protected static void setViewData(ViewData menuViewData) {
        if (PizzaMenuFragmentTab.thisViewData == null) {
            PizzaMenuFragmentTab.thisViewData = menuViewData;
        }
    }
}
