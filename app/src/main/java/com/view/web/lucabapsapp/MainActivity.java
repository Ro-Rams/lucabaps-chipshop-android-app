package com.view.web.lucabapsapp;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.file.i.o.FileLayer;
import com.listeners.TabListener;
import com.model.data.ViewData;
import com.model.json.parser.ExtractListsFromJson;
import com.model.json.parser.JsonParserLayer;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;


public class MainActivity extends Activity {

    private static final String url = "http://lucabapsjson.uphero.com/menus.json";
    private static final String filename = "menus.json";
    private ActionBar.Tab mainMenuTab;
    private ActionBar.Tab chineseMenuTab;
    private ActionBar.Tab pizzaMenuTab;
    private Fragment mainMenuFragmentTab;
    private Fragment chineseMenuFragmentTab;
    private Fragment pizzaMenuFragmentTab;
    private static boolean firstTime;

    static {
        firstTime = true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainMenuFragmentTab = new MainMenuFragmentTab();
        chineseMenuFragmentTab = new ChineseMenuFragmentTab();
        pizzaMenuFragmentTab = new PizzaMenuFragmentTab();
        setupTabs();
    }

    private void setupTabs() {
        // Asking for the default ActionBar element that our platform supports.
        ActionBar actionBar = getActionBar();
        getActionBar().setDisplayShowTitleEnabled(false);
        actionBar.setLogo(R.drawable.my_logo);

        // Screen handling while hiding ActionBar icon.
        actionBar.setDisplayShowHomeEnabled(true);
        // Screen handling while hiding Actionbar title.
        actionBar.setDisplayShowTitleEnabled(true);
        // Creating ActionBar tabs.
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        // Setting custom tab icons.
        mainMenuTab = actionBar.newTab().setText("Main");
        chineseMenuTab = actionBar.newTab().setText("Chinese");
        pizzaMenuTab = actionBar.newTab().setText("Pizza");
        // Setting tab listeners.
        mainMenuTab.setTabListener(new TabListener(mainMenuFragmentTab));
        chineseMenuTab.setTabListener(new TabListener(chineseMenuFragmentTab));
        pizzaMenuTab.setTabListener(new TabListener(pizzaMenuFragmentTab));
        // Adding tabs to the ActionBar.
        actionBar.addTab(mainMenuTab);
        actionBar.addTab(chineseMenuTab);
        actionBar.addTab(pizzaMenuTab);
        if (firstTime) {
            new RetrieveJsonTask(this).execute(url);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        //MenuInflater inflater = getMenuInflater();
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        this.closeOptionsMenu();
        Intent intent = new Intent(MainActivity.this, OurLocation.class);
        startActivity(intent);
        return true;
    }

    class RetrieveJsonTask extends AsyncTask<String, Void, Void> {

        private Activity activity;
        private ProgressDialog dialog;

        public RetrieveJsonTask(Activity activity) {
            this.activity = activity;
            dialog = new ProgressDialog(activity);
        }

        protected void onPreExecute() {
            this.dialog.setMessage("Getting menu data..");
            this.dialog.show();
        }

        protected Void doInBackground(String... urls) {
            try {
                JsonParserLayer parseLayer = new JsonParserLayer(url);
                FileLayer.CreateFile(filename, activity);
                setMainMenuData(parseLayer);
                setChineseData(parseLayer);
                setPizzaData(parseLayer);
                onPostExecute();
            } catch (JSONException e) {
                Log.e("MainActivity: ", e.getMessage());
            } catch (IOException ex) {
                Log.e("MainActivity: ", ex.getMessage());
                readJsonFromFile();
            } finally {
                return null;
            }
        }

        private void readJsonFromFile() {
            try {
                JsonParserLayer parseLayer = new JsonParserLayer(filename, activity);
                setMainMenuData(parseLayer);
                setChineseData(parseLayer);
                setPizzaData(parseLayer);
                onPostExecute();
            } catch (JSONException e) {
                Log.e("MainActivity: ", e.getMessage());
            } catch (IOException ex) {
                Log.e("MainActivity: ", ex.getMessage());
            }
        }

        private void setMainMenuData(JsonParserLayer parseLayer) {
            ExtractListsFromJson jsonLists = parseLayer.getJsonData("Main Menu");
            jsonLists.prepareData();
            ViewData mainMenuViewData = new ViewData(new ArrayList<>(jsonLists.getHeaders()), new HashMap<>(jsonLists.getChildren()), new HashMap<>(jsonLists.getChildrenSub()));
            MainMenuFragmentTab.setViewData(mainMenuViewData);
            MainMenuFragmentTab.success = true;
        }

        private void setChineseData(JsonParserLayer parseLayer) {
            ExtractListsFromJson jsonLists = parseLayer.getJsonData("Chinese Menu");
            jsonLists.prepareData();
            ViewData chineseMenuViewData = new ViewData(new ArrayList<>(jsonLists.getHeaders()), new HashMap<>(jsonLists.getChildren()), new HashMap<>(jsonLists.getChildrenSub()));
            ChineseMenuFragmentTab.setViewData(chineseMenuViewData);
            ChineseMenuFragmentTab.success = true;
        }

        private void setPizzaData(JsonParserLayer parseLayer) {
            ExtractListsFromJson jsonLists = parseLayer.getJsonData("Pizza Menu");
            jsonLists.prepareData();
            ViewData pizzaMenuViewData = new ViewData(new ArrayList<>(jsonLists.getHeaders()), new HashMap<>(jsonLists.getChildren()), new HashMap<>(jsonLists.getChildrenSub()));
            PizzaMenuFragmentTab.setViewData(pizzaMenuViewData);
            PizzaMenuFragmentTab.success = true;
        }

        protected void onPostExecute() {
            firstTime = false;
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }
    }
}


