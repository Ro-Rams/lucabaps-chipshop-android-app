package com.view.web.lucabapsapp;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;


public class OurLocation extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_our_location);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        ImageView img = (ImageView)findViewById(R.id.map);
        img.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse("https://www.google.co.uk/maps/place/158+Park+Rd,+Portadown,+Craigavon+BT62/@54.4242024,-6.4476456,17z/data=!3m1!4b1!4m2!3m1!1s0x4860efc4e0a09feb:0x4a11bae260a7ecd0"));
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_our_location, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        this.closeOptionsMenu();
        Intent intent = new Intent(OurLocation.this, MainActivity.class);
        startActivity(intent);
        return true;
    }
}
