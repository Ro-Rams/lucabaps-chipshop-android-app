package com.view.web.lucabapsapp;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.listeners.ExpandableListAdapter;
import com.model.data.ViewData;

public class MainMenuFragmentTab extends Fragment {

    private ExpandableListAdapter listAdapter;
    private ExpandableListView expListView;
    protected static boolean success;
    private static ViewData thisViewData;

    static {
        success = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.main_menu_fragment, container, false);
        expListView = (ExpandableListView) rootView.findViewById(R.id.lvExp);
        while (!success) {
        }
        listAdapter = new ExpandableListAdapter(this,expListView, MainMenuFragmentTab.thisViewData.getListDataHeader(), MainMenuFragmentTab.thisViewData.getListDataChild(), MainMenuFragmentTab.thisViewData.getListDataChildSub());
        expListView.setAdapter(listAdapter);
        return rootView;
    }

    protected static void setViewData(ViewData menuViewData) {
        if (MainMenuFragmentTab.thisViewData == null) {
            MainMenuFragmentTab.thisViewData = menuViewData;
        }
    }
}
